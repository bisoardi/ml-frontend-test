import React, { Component } from "react";
import queryString from 'query-string';
import { Container, Row, Col } from 'react-bootstrap';
import '../scss/items.scss';

const axios = require('axios');

class Items extends Component {
  constructor(props) {
    super(props);
    this.state = { value: { "categories": [], "items": [] }, isFetching: true };
  }

  componentWillMount() {
    const query = queryString.parse(this.props.location.search);
    this.getItems(query.search);
  }

  getItems(query) {

    var url = `/api/items?q=${query}`;
    axios.get(url)
      .then(response => {
        this.setState({ value: response.data });
        this.setState({ isFetching: false });
      })
      .catch(error => {
        console.log(error)
      });
  }

  render() {
    if (this.state.isFetching)
      return null
    else
      return (
        <Container className="ml-container" fluid="true">
          <Row className="ml-item-categories">
            { this.state.value.categories.map((category, i)  => <span key={i}> { category } > </span>) }
          </Row>
          <Row>
            <Col sm="12" md={{ size: 12, offset: 3 }}> { <Products items={ this.state.value.items.slice(0, 4)} /> } </Col>
          </Row>
        </Container>
      );
  }
}

export default Items;

const Products = ({ items }) => (
  <div>{ items.map(item => <Item key={item.id} {...item} />) }</div>
);

const Item = ({ id, title, price, picture, free_shipping, address }) => (
  <div>
    <a href={'/items/'+id}><img src={ picture } alt={ title } /></a>
    <span className="ml-price">  ${price.amount}</span>
    <div className="ml-title">
      <label> <a href={'/items/'+id}> { title } </a></label>
    </div>
    <div className="ml-free-shipping">
      <label> { free_shipping === true ? <span> '' </span> : '' } </label>
    </div>
      <div className="ml-province">
      <label> { address } </label>
    </div>
    <hr />
  </div>
);
