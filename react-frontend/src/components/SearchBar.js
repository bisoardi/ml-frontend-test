import React, { Component }from "react";
import '../scss/SearchBar.scss'
import { Container, Row } from 'react-bootstrap';
class SearchBar extends Component {

  render() {
    return (
      <header className="ml-search-bar">
        <Container className="search-container">
          <Row>
            <form action="/items">
              <a href="/"></a>
              <input type="text" placeholder="Nunca dejes de buscar" name="search" />
              <button type="submit"></button>
            </form>
          </Row>
        </Container>
      </header>
    );
  }
}

export default SearchBar;


