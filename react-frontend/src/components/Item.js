import React, { Component } from "react";
import { Container, Row } from 'react-bootstrap';
import '../scss/items.scss';

const axios = require('axios');

class Item extends Component {

  constructor(props) {
    super(props);
    this.state = { value: { "categories": [], "item": [] }, desc: "", isFetching: true };
  }

  componentWillMount() {
    const id = this.props.match.params.id;
    this.getItem(id);
  }

  getItem(id) {

    var url = `/api/items/${id}`;
    axios.get(url)
      .then(response => {
        this.setState({ value: response.data });
        this.getDescription(id);
      })
      .catch(error => {
        console.log(error)
      });
  }

  getDescription(id) {

    var url = `/api/items/${id}/descriptions`;
    axios.get(url)
      .then(response => {
        this.setState({ desc: response.data });
        this.setState({ isFetching: false })
        console.log(this.state.value.item);
      })
      .catch(error => {
        console.log(error)
      });
  }

  render() {
    if (this.state.isFetching) return null
    else 
    return (
      <Container className="ml-container" fluid="true">
        <Row className="ml-item-categories">
          { this.state.value.categories.map((category, i)  => <span key={i}> { category } > </span>) }
        </Row>
        <Row>
          <div className="ml-item-img">
            <img src={ this.state.value.item.picture } alt={ this.state.value.item.title } />
          </div>
          <div className="ml-item-condition">
            <label>{ this.state.value.item.condition } - { this.state.value.item.sold_quantity } vendidos </label>
          </div>
          <div className="ml-item-title">
            <label> { this.state.value.item.title } </label>
          </div>
          <div className="ml-item-price">
            <label> $ { this.state.value.item.price.amount } </label>
          </div>
          <div className="ml-item-button">
            <button> Comprar </button>
          </div>
          <div className="ml-item-description">
            <label> Descripción del Producto </label>
            <p> { this.state.desc } </p>
          </div>
        </Row>
      </Container>
  );
  }
}

export default Item;
