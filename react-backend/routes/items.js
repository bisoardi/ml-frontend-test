var express = require('express');
var router = express.Router();
var services = require('../services/services');


/* GET items listing. */
router.get('/', (req, res) => {
  services.fetchItemsData(req.query.q)
  .then(data => {
    res.json(data);
  })
});

/* GET item by id. */
router.get(['/:id', '/:id/:descriptions'], (req, res) => {
   if (!req.params.descriptions) {
    services.fetchItemByID(req.params.id)
  .then(data => {
    res.json(data);
  })
  .catch(err => console.log(err))
  } else {
    services.fetchDescription(req.params)
  .then(data => {
    res.json(data);
  })
  .catch(err => console.log(err))
  }
});

module.exports = router;